﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sol_EnableViewState_Dynamic
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack == false)
            {
                Response.Write("Tanmay Joshi,25");



                lblName1.Text = "Tanmay";
                lblName2.Text = "Joshi";
                lblName3.Text = "25";


            }
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            lblName1.EnableViewState = false;
            lblName2.EnableViewState = true;
            lblName3.EnableViewState = false;
        }

    }
}
